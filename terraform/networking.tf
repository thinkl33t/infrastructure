resource "openstack_networking_network_v2" "baserock_network" {
  name           = "Baserock Network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "baserock_subnet" {
  name       = "Baserock Subnet"
  network_id = "${openstack_networking_network_v2.baserock_network.id}"
  cidr       = "10.3.0.0/24"
  ip_version = 4
}


data "openstack_networking_network_v2" "external_network" {
  name = "ext-net"
}

resource "openstack_networking_router_v2" "baserock_router" {
  name                = "Baserock Router"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.external_network.id
}

resource "openstack_networking_router_interface_v2" "baserock_router_interface" {
  router_id = "${openstack_networking_router_v2.baserock_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.baserock_subnet.id}"
}

# Security groups

resource "openstack_networking_secgroup_v2" "sg_base" {
  name        = "base"
  description = "Allow all outgoing traffic, and allow incoming ICMP (ping) and SSH connections"
  delete_default_rules = "true"
}

resource "openstack_networking_secgroup_rule_v2" "sg_base_egress_icmp" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_base.id}"
}

resource "openstack_networking_secgroup_rule_v2" "sg_base_egress_any" {
  direction         = "egress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_base.id}"
}

resource "openstack_networking_secgroup_rule_v2" "sg_base_egress_any_v6" {
  direction         = "egress"
  ethertype         = "IPv6"
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_base.id}"
}

resource "openstack_networking_secgroup_rule_v2" "sg_base_ingress_icmp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_base.id}"
}


resource "openstack_networking_secgroup_rule_v2" "sg_base_ingress_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_base.id}"
}



resource "openstack_networking_secgroup_v2" "sg_haste_server" {
  name        = "haste-server"
  description = "Allow incoming TCP requests for haste server"
  delete_default_rules = "true"
}

resource "openstack_networking_secgroup_rule_v2" "sg_haste_server_main" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 7777
  port_range_max    = 7777
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_haste_server.id}"
}

resource "openstack_networking_secgroup_v2" "sg_gitlab_bot" {
  name        = "gitlab-bot"
  description = "Allow incoming TCP requests for gitlab-bot"
  delete_default_rules = "true"
}


resource "openstack_networking_secgroup_rule_v2" "sg_gitlab_bot_main" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1337
  port_range_max    = 1337
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_gitlab_bot.id}"
}


resource "openstack_networking_secgroup_v2" "sg_git_server" {
  name        = "git-server"
  description = "Allow inbound SSH, HTTP, HTTPS and Git requests."
  delete_default_rules = "true"
}

resource "openstack_networking_secgroup_rule_v2" "sg_git_server_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_git_server.id}"
}

resource "openstack_networking_secgroup_rule_v2" "sg_git_server_https" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_git_server.id}"
}

resource "openstack_networking_secgroup_rule_v2" "sg_git_server_git" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9418
  port_range_max    = 9418
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_git_server.id}"
}



resource "openstack_networking_secgroup_v2" "sg_shared_artifact_cache" {
  name        = "shared-artifact-cache"
  description = "Allow inbound HTTP, HTTPS and ostree-over-SSH (which I've assigned to port 22200)"
  delete_default_rules = "true"
}

resource "openstack_networking_secgroup_rule_v2" "sg_shared_artifact_cache_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_shared_artifact_cache.id}"
}
resource "openstack_networking_secgroup_rule_v2" "sg_shared_artifact_cache_https" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_shared_artifact_cache.id}"
}
resource "openstack_networking_secgroup_rule_v2" "sg_shared_artifact_cache_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22200
  port_range_max    = 22200
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_shared_artifact_cache.id}"
}


resource "openstack_networking_secgroup_v2" "sg_web_server" {
  name        = "web-server"
  description = "Allow inbound HTTP, HTTPS and ostree-over-SSH (which I've assigned to port 22200)"
  delete_default_rules = "true"
}


resource "openstack_networking_secgroup_rule_v2" "sg_web_server_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_web_server.id}"
}
resource "openstack_networking_secgroup_rule_v2" "sg_web_server_https" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.sg_web_server.id}"
}

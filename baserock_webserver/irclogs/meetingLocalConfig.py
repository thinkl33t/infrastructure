# Richard Darst, July 2009
#
# Minimal meetingLocalConfig.py
#
# This file is released into the public domain, or released under the
# supybot license in areas where releasing into the public domain is
# not possible.
#

class Config(object):
    # These are "required":
    logFileDir = '/home/supybot/meetings/'
    logUrlPrefix = 'https://irclogs.baserock.org/meetings/'

    # These, you might want to change:
    #MeetBotInfoURL = 'http://wiki.debian.org/MeetBot'
    filenamePattern = '%(channel)s/%%Y/%%m/%(channel)s.%%F-%%H.%%M'
